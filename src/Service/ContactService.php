<?php

namespace App\Service;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\Configuration;
use Doctrine\DBAL\DriverManager;

class ContactService
{
    /**
     * @var
     */
    private $configuration;

    /**
     * Connection constructor.
     *
     * @param $noEvosys
     *
     * @throws \Exception
     */
    public function __construct($noEvosys)
    {
        $doctrineConfig = new Configuration();

        if (!isset(ConnectionProvider::DETAILS[$noEvosys])) {
            throw new \Exception('You have enter wrong detail number!');
        }

        try {
            $details = ConnectionProvider::DETAILS[$noEvosys];

            return $this->configuration = DriverManager::getConnection($details, $doctrineConfig);
        } catch (DBALException $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @param $code
     *
     * @throws DBALException
     *
     * @return array|bool
     */
    public function fetchSubscriberByContact($id, $code)
    {
        return $this->configuration->fetchAssoc("SELECT contact.contactID, cc.LanguageID, cc.stopEmails
  FROM dbo.tbl_contacts as contact
  INNER JOIN  dbo.tbl_contacts_common  as cc
  ON contact.contactID = cc.contactID
  WHERE contact.contactID = '$id' AND contact.code = '$code' AND (cc.stopEmails = 0 OR cc.stopEmails IS null)");
    }

    /**
     * @param string $sql
     *
     * @throws DBALException
     *
     * @return \Doctrine\DBAL\Driver\ResultStatement
     */
    public function execute(string $sql)
    {
        return $this->configuration->executeQuery($sql);
    }

    /**
     * @param $contactID
     *
     * @throws DBALException
     *
     * @return bool
     */
    public function updateContactToUnsubscribe($contactID)
    {
        $this->execute("UPDATE dbo.tbl_contacts_common SET stopEmails = 1, ModifUsr = 'SELF' WHERE contactID = '$contactID'");

        $this->execute("UPDATE dbo.tbl_contacts SET Modified = getDate() WHERE contactID = '$contactID'");

        return true;
    }

    /**
     * @param $language
     *
     * @return array
     */
    public function findCountriesByLanguage($language)
    {
        return $this->configuration->fetchAll('SELECT CountryID, Country'.$language.' FROM dbo.tbl_countries');
    }

    /**
     * @param array $formData
     * @param $contactId
     * @param $addressID
     *
     * @throws DBALException
     */
    public function updateContactDetails(array $formData, $contactId, $addressID)
    {
        $this->execute("UPDATE dbo.tbl_contacts SET Name1 = '$formData[name]', 
            Name2 = '$formData[lastname]' WHERE contactID = '$contactId'");

        if ($formData['language']) {
            $this->execute("UPDATE dbo.tbl_contacts_common SET Politesse = '$formData[politesse]',StopEmails = '$formData[choix]', Title = '$formData[function]', LanguageID = '$formData[language]' WHERE contactID = '$contactId'");
        } else {
            $this->execute("UPDATE dbo.tbl_contacts_common SET Politesse = '$formData[politesse]',StopEmails = '$formData[choix]', Title = '$formData[function]' WHERE contactID = '$contactId'");
        }

        $this->execute("UPDATE dbo.tbl_addresses SET Address1 = '$formData[adresse1]', CountryID = '$formData[country]',
              Address2 = '$formData[adresse2]', ZipCode = '$formData[postal]', Locality = '$formData[locality]' WHERE AddressID = '$addressID'");
    }

    /**
     * @param $noEvosys
     *
     * @throws DBALException
     *
     * @return array|bool
     */
    public function findClientLanguage($noEvosys)
    {
        return $this->configuration->fetchAssoc("SELECT [LNG] FROM dbo.tbl_parameters_general WHERE NoEvosys = '$noEvosys'");
    }

    /**
     * @param $contactId
     * @param $code
     *
     * @throws DBALException
     *
     * @return array|bool
     */
    public function fetchUserByContactIdAndCode($contactId, $code)
    {
        return  $this->configuration->fetchAssoc("SELECT contact.Name1, contact.Name2, cc.StopEmails ,cc.Title, cc.LanguageID, cc.Politesse, a.Address1, a.Address2 , a.ZipCode ,a.Locality, a.CountryID, cc.AddressID
    FROM dbo.tbl_contacts as contact
    INNER JOIN  dbo.tbl_contacts_common  as cc ON contact.contactID = cc.contactID 
    INNER JOIN  dbo.tbl_addresses  as a ON cc.AddressID = a.AddressID
    WHERE contact.contactID = '$contactId' AND contact.Code = '$code'");
    }

    /**
     * @return array
     */
    public function findPolitesse()
    {
        return $this->configuration->fetchAll('select p.PolitesseID, l.NameID, l.FR,l.EN, l.NL FROM dbo.tbl_languages as l 
 INNER JOIN  dbo.tbl_politesses as p ON p.NameID = l.NameID ');
    }

    public function findCommunication($id)
    {
        return $this->configuration->fetchAll("SELECT cc.ID, cc.PhoneAll, cc.ContactID, cc.CommunicationID, cc.Position, ct.Code FROM dbo.tbl_contacts_communications as cc 
  LEFT JOIN dbo.tbl_communication_types as ct ON cc.CommunicationID = ct.CommunicationID 
  WHERE ContactID = '$id';");
    }

    public function insertCommunicationDetails($contacts, $id, $communicationID)
    {
        $i = 1;
        foreach ($contacts as $contact) {
            $this->execute("INSERT INTO dbo.tbl_contacts_communications 
        (ContactID, CommunicationID, Communication,Description,  Main, PhoneAll, Position) 
        VALUES ('$id', '$communicationID', '$contact', NULL, 0, '$contact', '$i')");
            ++$i;
        }
    }

    public function getCommunicationIdByCode($code)
    {
        return $this->configuration->fetchAssoc("SELECT CommunicationID FROM tbl_communication_types WHERE Code = '$code'");
    }

    public function removeCommunicationContact($contact)
    {
        if (is_array($contact)) {
            foreach ($contact as $k => $data) {
                $this->execute("DELETE FROM dbo.tbl_contacts_communications WHERE ID = '$k';");
            }
        } else {
            return $this->execute("DELETE FROM dbo.tbl_contacts_communications WHERE ID = '$contact';");
        }
    }
}
