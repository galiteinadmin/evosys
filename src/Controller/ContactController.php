<?php

namespace App\Controller;

use App\Service\ContactService;
use Doctrine\DBAL\DBALException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact/unsubscribe", name="unsubscribe", methods={"GET"})
     *
     * @param Request $request
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function unsubscribe(Request $request)
    {
        $data     = $request->query;
        if (empty($data->get('id')) || empty($data->get('code')) || empty($data->get('noevosys'))) {
            throw new \Exception("You have missing parameter 'id', 'code' or 'noevosys'!");
        }

        $id       = $data->get('id');
        $code     = $data->get('code');
        $noEvosys = $data->get('noevosys');

        $connection       = new ContactService($noEvosys);
        $language         = $connection->findClientLanguage($noEvosys)['LNG'];
        $contactSubscribe = $connection->fetchSubscriberByContact($id, $code);

        if ($contactSubscribe) {
            $connection->updateContactToUnsubscribe($contactSubscribe['contactID']);
            if ('FR' == $language) {
                $message = 'Vous avez été désinscrit';
            } elseif ('NL' == $language) {
                $message = 'Je bent uitgeschreven';
            } elseif ('EN' == $language) {
                $message = 'You have been unsubscribed';
            }
        } else {
            if ('FR' == $language) {
                $message = 'Votre profil a déjà été désinscrit';
            } elseif ('NL' == $language) {
                $message = 'Je profiel is al uitgeschreven';
            } elseif ('EN' == $language) {
                $message = 'Your profile has already been unsubscribed';
            }
        }

        return $this->render('contact/unsubscribe.html.twig', [
            'MESSAGE'  => $message,
            'NoClient' => $noEvosys,
        ]);
    }

    /**
     * @Route("/contact/personal-data", name="personal_data", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function personalData(Request $request)
    {
        $getData           = $request->query;
        $id                = $getData->get('id');
        $code              = $getData->get('code');
        $noEvosys          = $getData->get('noevosys');
        $language          = $getData->get('language');

        if (empty($getData->get('id')) ||
            empty($getData->get('code')) ||
            empty($getData->get('noevosys'))) {
            throw new \Exception("You have missing parameter 'id' or 'code' or 'noevosys'!");
        }

        $connection     = new ContactService($noEvosys);

        if (!$language) {
            $language = $connection->findClientLanguage($noEvosys)['LNG'];
        }

        $tpl      = 'contact/unsubscribe.html.twig';
        if ('FR' == $language) {
            $message = "Votre profiln'existe pas dans notre base de données";
        } elseif ('NL' == $language) {
            $message = 'Uw profiel bestaat niet in onze database';
        } elseif ('EN' == $language) {
            $message = 'Your profile does not exist in our database';
        }

        $response = [
            'MESSAGE'  => $message,
            'NoClient' => $noEvosys,
            'lng'      => $language,
        ];

        try {
            $userInformation = $connection->fetchUserByContactIdAndCode($id, $code);
            if (!$getData->get('language')) {
                $language        = $userInformation['LanguageID'];
            }
        } catch (DBALException $exception) {
            return $this->render($tpl, $response);
        }

        $communication   = $connection->findCommunication($id);

        if ($userInformation) {
            $politesse         = $connection->findPolitesse();
            $formData          = $request->request->all();
            $formData          = $this->removeExtraStringFromString($formData);

            if (isset($formData['submit']) && $formData['submit']) {
                if (empty($formData['country'])) {
                    $formData['country'] = 'BE';
                }

                if (isset($formData['choix'])) {
                    $formData['choix'] = 0;
                } else {
                    $formData['choix'] = 1;
                }

                try {
                    $connection->updateContactDetails($formData, $id, $userInformation['AddressID'], $language);

                    if (isset($formData['emailedit'])) {
                        if (isset($formData['email'])) {
                            $formData['email'] = array_merge($formData['email'], $formData['emailedit']);
                        } else {
                            $formData['email'] = $formData['emailedit'];
                        }

                        $connection->removeCommunicationContact($formData['emailedit']);
                    }

                    if (isset($formData['telephoneedit'])) {
                        if (isset($formData['telephone'])) {
                            $formData['telephone'] = array_merge($formData['telephone'], $formData['telephoneedit']);
                        } else {
                            $formData['telephone'] = $formData['telephoneedit'];
                        }

                        $connection->removeCommunicationContact($formData['telephoneedit']);
                    }

                    $telephone = $connection->getCommunicationIdByCode('T');
                    $email     = $connection->getCommunicationIdByCode('E');

                    if (isset($formData['telephone'])) {
                        $connection->insertCommunicationDetails($formData['telephone'], $id, $telephone['CommunicationID']);
                    }

                    if (isset($formData['email'])) {
                        $connection->insertCommunicationDetails($formData['email'], $id, $email['CommunicationID']);
                    }

                    return $this->redirectToRoute('personal_data', ['id' => $id, 'code' => $code, 'noevosys' => $noEvosys]);
                } catch (\Exception $exception) {
                    return $exception->getMessage();
                }
            }

            $countries = $connection->findCountriesByLanguage($language);
            $tpl       = 'contact/personal_data.html.twig';
            $response  = [
                'NoClient'         => $noEvosys,
                'code'             => $code,
                'id'               => $id,
                'userInformation'  => $userInformation,
                'communication'    => $communication,
                'politesse'        => $politesse,
                'countries'        => $countries,
                'lng'              => $language,
                'data'             => $getData,
            ];
        }

        return $this->render($tpl, $response);
    }

    /**
     * @Route("/contact/remove-comunication/{contactId}/{noEvosys}/{code}/{id}", name="remove_communication", methods={"GET", "POST"})
     *
     * @param $contactId
     * @param $noEvosys
     * @param $code
     * @param $id
     *
     * @throws \Exception
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeCommunication($contactId, $noEvosys, $code, $id)
    {
        $connection     = new ContactService($noEvosys);
        try {
            $connection->removeCommunicationContact($contactId);
        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('personal_data', ['id' => $id, 'code' => $code, 'noevosys' => $noEvosys]);
    }

    /**
     * @param array $dataArray
     *
     * @return array
     */
    private function removeExtraStringFromString(array $dataArray)
    {
        foreach ($dataArray as $k => $item) {
            if (is_string($item)) {
                $dataArray[$k] = $this->tripSearchHighlight($item);
            }
        }

        return $dataArray;
    }

    /**
     * @param $string
     *
     * @return array|string
     */
    private function tripSearchHighlight($string)
    {
        $string = stripslashes(trim($string, '"'));
        $string = str_replace('“', '', ltrim($string, ' '));
        $string = str_replace('”', '', $string);
        $string = preg_replace('/^(\'(.*)\'|"(.*)")$/', '$2$3', $string);
        $string = preg_replace('/(^[\"\']|[\"\']$)/', '', $string);
        $string = strip_tags($string);
        $string = str_replace("'", "''", $string);

        return $string;
    }
}
