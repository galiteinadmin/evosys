<?php

if (class_exists('PhpCsFixer\Finder')) {    // PHP-CS-Fixer 2.x
    $finder = PhpCsFixer\Finder::create()
        ->in(__DIR__)
    ;

    return PhpCsFixer\Config::create()
        ->setRules(array(
             '@Symfony' => true,
             'binary_operator_spaces' => ['align_double_arrow' => true,
             'align_equals' => true],
               'array_syntax' => ['syntax' => 'short'],
             'blank_line_after_opening_tag' => true,
             'phpdoc_order' => true,
        ))
        ->setFinder($finder)
    ;
} elseif (class_exists('Symfony\CS\Finder\DefaultFinder')) {

    $finder = Symfony\CS\Finder\DefaultFinder::create()
        ->exclude('data')
        ->exclude('vendor')
        ->exclude('web')
        ->in(__DIR__);

    return Symfony\CS\Config\Config::create()
        ->level(Symfony\CS\FixerInterface::SYMFONY_LEVEL)
        ->fixers([
            'psr7' => true,
            'align_double_arrow',
            'align_equals',
            'multiline_spaces_before_semicolon',
            'short_array_syntax',
            'newline_after_open_tag',
            'ordered_use',
            'phpdoc_order',
        ])
        ->finder($finder);
}